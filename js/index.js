/* Bai- 1: Cho người dùng nhập 3 số nguyên.
Viết chương trình xuất 3 số theo thứ tự tăng dần */

function Bai1() {
    var so1 = document.getElementById("so1").value * 1;
    var so2 = document.getElementById("so2").value * 1;
    var so3 = document.getElementById("so3").value * 1;
    var result = "";
    if (so1 <= so2 && so1 <= so3) {
        result += so1 + " ; ";
        result += (so2 <= so3) ? (so2 + " ; " + so3) : (so3 + " ; " + so2);
        // if (so2 <= so3) {
        //     result += so2 + " ; " + so3;
        // }
        // else {
        //     result += so3 + " ; " + so2;
        // }
    } else if (so2 <= so1 && so2 <= so3) {
        result += so2 + " ; ";
        result += (so1 <= so3) ? (so1 + " ; " + so3) : (so3 + " ; " + so1);
        // if (so1 <= so3) {
        //     result += so1 + " ; " + so3;
        // }
        // else {
        //     result += so3 + " ; " + so1;
        // }
    } else {
        result += so3 + " ; ";
        result += (so1 <= so2) ? (so1 + " ; " + so2) : (so2 + " ; " + so1);
        // if (so1 <= so2) {
        //     result += so1 + " ; " + so2;
        // }
        // else {
        //     result += so2 + " ; " + so1;
        // }
    }
    document.getElementById("ketqua_bai1").innerHTML = `<h2>${result}</h2>`;
}

/* Bai 2: Viết chương trình chào hỏi thành viên 
Sau đó dựa vào câu trả lời và đưa ra lời chào phù hợp.
Giả sử trong gia đình có 4 thành viên: Bố(B), Mẹ(M), anh Trai(A), Em Gái(E).
*/

function Bai2() {

    var thanhVien = document.getElementById("thanhvien").value;
    var result;
    switch (thanhVien) {
        case "b": result = "Xin Chào Bố"; break;
        case "m": result = "Xin Chào Mẹ"; break;
        case "a": result = "Xin Chào Anh Trai"; break;
        case "e": result = "Xin Chào Em Gái"; break;
        default: result = "Chọn Thành Viên !!!";
    }

    document.getElementById("ketqua_bai2").innerHTML = `<h2>${result}</h2>`;
}

/* Bai 3: Cho 3 số nguyên. Viết chương trình xuất ra có
 bao nhiêu số lẻ và bao nhiêu số chẵn. */

function Bai3() {
    var sNguyen1 = document.getElementById("songuyen1").value * 1;
    var sNguyen2 = document.getElementById("songuyen2").value * 1;
    var sNguyen3 = document.getElementById("songuyen3").value * 1;
    var sChan = 0, sLe = 0;

    (sNguyen1 % 2 == 0) ? sChan++ : sLe++;
    (sNguyen2 % 2 == 0) ? sChan++ : sLe++;
    (sNguyen3 % 2 == 0) ? sChan++ : sLe++;


    document.getElementById("ketqua_bai3").innerHTML =
        `<h2>Tổng số Chẵn là ${sChan}</h2>
        <h2>Tổng số Lẻ là ${sLe}</h2>`;
}

/* Bài 4: Viết chương trình cho nhập 3 cạnh của tam giác.
Hãy cho biết đó là tam giác gì?
 */

function Bai4() {
    var canh1 = document.getElementById("canh1").value * 1;
    var canh2 = document.getElementById("canh2").value * 1;
    var canh3 = document.getElementById("canh3").value * 1;
    var result;
    if (canh1 == canh2 == canh3)
        result = "Tam Giác Đều";
    else if (canh1 == canh2 || canh2 == canh3 || canh1 == canh3) {
        result = "Tam Giác Cân";

    } else if (canh1 * canh1 + canh2 * canh2 == canh3 * canh3
        || canh2 * canh2 + canh3 * canh3 == canh1 * canh1
        || canh3 * canh3 + canh1 * canh1 == canh2 * canh2) {
        result = "Tam Giác Vuông";
    }
    else
        result = "Tam Giác Thường";

    if (canh1 == 0 || canh2 == 0 || canh3 == 0)
        result = "3 Cạnh Không Phải Tam Giác";
    document.getElementById("ketqua_bai4").innerHTML = `<h2>${result}</h2>`;
}